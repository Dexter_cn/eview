require('./main.css');
import { Router, Route, hashHistory } from 'react-router';

let React     = require('react');
let ReactDOM  = require('react-dom');

let Header     =  require('./components/header/header.js');
let Home       =  require('./components/home/home.js');
let Products   =  require('./components/Products/Products.js');
let Aboutus    =  require('./components/aboutus/aboutus.js');
let Footer     =  require('./components/footer/footer.js');
let Footernone =  require('./components/footer/footer_none.js');




ReactDOM.render(<Header type="top-mune" />, document.getElementById('header'));

ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={Home}/>
    <Route path="/about" component={Aboutus}/>
    <Route path="/down"  component={Header}/>
    <Route path="/Products(/:name)"  component={Products}/>
  </Router>
), document.getElementById('aritcle'));


ReactDOM.render((
  <Router history={hashHistory}>
    <Route path="/" component={Footer} />	
    <Route path="**" component={Footernone} />
  </Router>
), document.getElementById('footer'));






