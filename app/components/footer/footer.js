var React = require('react');
import styles from './footer.css';

//ES6
class Footer extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="footer-title">Get in Touch</div>
                <div className="row">
                	<div className="col-md-6">
                		<ul>
                			<li>
                				<img src="img/footer/emali.png" alt="" />
                				<p>Sales Department : sales@eviewltd.com / info@eviewltd.com</p>
                				<p>Technical Support : support@eviewltd.com </p>
                			</li>
                			<li>
                				<img src="img/footer/phone.png" alt="" />
                				<p>+86-755-23772735/28073450 </p>
                				<p>+86-755-33100549</p>
                				<p>Emergency call : +86-18620310172</p>
                			</li>
                			<li>
                				<img src="img/footer/zz.png" alt="" />
                				<p>+86-755-28073450</p>
                			</li>
                		</ul>
                		<div className="someLink">
                			<p>
                				<img src="img/logo.png" alt="" />
                				Eview Industrial Limited
                			</p>
                			<ul>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_f.png" alt="" /></a></li>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_t.png" alt="" /></a></li>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_w.png" alt="" /></a></li>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_text.png" alt="" /></a></li>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_in.png" alt="" /></a></li>
                				<li><a href="javasript:void(0)"><img src="img/footer/link_s.png" alt="" /></a></li>
                			</ul>
                		</div>
                	</div>
                	<div className="col-md-6">
                		<form role="form">
                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="Name">Name</label>
                		    <input type="text" className="form-control" id="Name" placeholder="Enter Name" />
                		  </div>
                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="email">Email address</label>
                		    <input type="text" className="form-control" id="email" placeholder="Enter email" />
                		  </div>
                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="number">Number</label>
                		    <input type="text" className="form-control" id="number" placeholder="Enter Number" />
                		  </div>

                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="company">Company</label>
                		    <input type="text" className="form-control" id="company" placeholder="Enter Company" />
                		  </div>
                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="Wwebsite">Website</label>
                		    <input type="text" className="form-control" id="website" placeholder="Enter Website" />
                		  </div>
                		  <div className="form-group col-sm-4">
                		  	<label  htmlFor="yourTitle">Your Title</label>
                		    <input type="text" className="form-control" id="yourTitle" placeholder="Enter Your Title" />
                		  </div>

                		  <div className="form-group col-sm-12">
                		  	<label  htmlFor="centralStation">Central Station</label>
                		    <input type="text" className="form-control" id="centralStation" placeholder="Enter Your Central Station" />
                		  </div>

                		  <div className="form-group col-sm-12">
                		  	<label  htmlFor="Message">Message</label>
                		    <textarea  rows="6" type="text" className="form-control" id="Message" placeholder="Enter Message" >
                		    </textarea>
                		  </div>
                		  <button type="submit" className="btn btn-default btn-lg btn-block">Submit</button> 
                		</form>
                	</div>
                </div>
            </div>
        );
    }
}



module.exports = Footer;