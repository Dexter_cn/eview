var React = require('react');
import styles from './header.css'; 
//ES6

var data = [
    {
        "firstMune":"HOME",
        "link":'#',

        "secondMune" : '',
    },
    {
        "firstMune"  : "PRODUCTS",
        "link":'javascript:void(0)',

        "secondMune" : ['EV-07S','EV-09','EV-200'],
        "secondLink" : ['#/Products/EV07S','#/Products/EV09','#/Products/EV200'],
    },
    {
        "firstMune":"ABOUT US",
        "link":'#/about',

        "secondMune" : '',
    },
    {
        "firstMune":"DOWNLOADS",
        "link":'#/down',

        "secondMune" :'',
    },
    {
        "firstMune":"GPS LOGIN",
        "link":'http://www.smart-tracking.com/login.jsp',

        "secondMune" : '',
    }
];

//一级目录的模块
class MuneitemFirst extends React.Component {
    render() {
        return <ul className={this.props.stateNow}>{
            this.props.data.map((e,index) => {
              return (
                <li key={index} >
                    <a href={e.link} >{e.firstMune}</a>
                    {e.secondMune == '' ? null:<MuneitemSecond  data={e.secondMune} link={e.secondLink}  />}
                </li>
            )})
        }</ul>
    }
}

//二级目录的模块
class MuneitemSecond extends React.Component {
    render() {
        return (
            <ul className='hide' >
                {this.props.data.map((e,index) => {
                    return (
                        <li  key={index} ><a href={this.props.link[index]} >{e}</a></li>
                    )
                })}
            </ul>
        );
    }
}


//一二级目录拼接的木块
class Header extends React.Component {
    constructor(props) {
       super(props);
       this.state = { stateNow : true };
       this.muneClick = this.muneClick.bind(this);
     }

     muneClick() {
         this.setState({stateNow: !this.state.stateNow});
       }

    render() {
        const firstMuneState = this.state.stateNow ? 'hide768' : '';
        return (
            <nav className={"container "+this.props.type}>
              <img className="logo" src="./img/logo.png" alt="" />
              <a href="#"  onClick={this.muneClick}><h4>Eview&CO.</h4></a>
              {<MuneitemFirst data={data} stateNow={firstMuneState} />}
            </nav>
        );
    } 
}

module.exports = Header;


// e.secondMune.map((icc,indexx) => { 
//                         return <li key={indexx}> <a href="javascript:void(0)" >{icc}</a></li>
//                     })