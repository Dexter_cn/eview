var React = require('react');

let Intro   = require('./intro/intro.js');
let Brief   = require('./brief/brief.js');
let Trait   = require('./trait/trait.js');
let Right   = require('./right/right.js');
let Details = require('./details/details.js');


//ES6
class Products extends React.Component {
    render() {
        return (
        	<div id="products" className="contentFirst" >
               <Intro   data={this.props.params.name} />
               <Brief   data={this.props.params.name} />
               <Trait   data={this.props.params.name} />
               <Right   data={this.props.params.name} />
               <Details data={this.props.params.name} />
            </div>
        );
    }
}

module.exports = Products; 