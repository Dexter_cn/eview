var React = require('react');
import styles from './right.css'; 


var EV07S = [
    {
        "img"   : "9.png",
        "content" :[
            {
                "name":"Elderly",
                "img":"10.png"
            },
            {
                "name":"Patients",
                "img":"11.png"
            },
            {
                "name":"Teens",
                "img":"12.png"
            },
            {
                "name":"Children",
                "img":"13.png"
            },
            {
                "name":"Travelers",
                "img":"14.png"
            },
            {
                "name":"Employees",
                "img":"15.png"
            },
            {
                "name":"Lone workers",
                "img":"16.png"
            }
        ]
    }
]

var EV09 = [
    {
        "img"   : "9.png",
        "content" :[
            {
                "name":"For  Person",
                "img":"10.png"
            },
            {
                "name":"For  Pet",
                "img":"11.png"
            },
            {
                "name":"For Bicycle",
                "img":"12.png"
            }
        ]
    }
]

var EV200 = '';


class Right extends React.Component {

    render() {
        let data = this.props.data == 'EV07S' ? EV07S : (this.props.data== 'EV09' ? EV09 : EV200);
        return(
            <div id="right">
                {data == ''?'':<Evall data={this.props.data} array={data} />}
            </div>
            
        )
    }
}


class Evall extends React.Component {
    render(){
        return (
            <div>
                {
                    this.props.array.map((e,index)=>{
                    return (
                        <div key={index}>
                            <div  className="container">
                                <div className="row">
                                    <div className="title"><span>02</span> Perfect For Tracking</div>
                                    <div className="col-sm-5"><img src={"img/products/"+this.props.data+"/"+e.img} alt="" /></div>
                                    <div className="col-sm-7">
                                        <div className="row right-img"> 
                                            {e.content.map((e,index) =>{
                                                return (
                                                    <div className={this.props.data=='EV07S'?'col-sm-3':'col-sm-4 marginTop'}  key={index}>
                                                        <img src={"img/products/"+this.props.data+"/"+e.img} alt="" />
                                                        <p>{e.name}</p>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}






module.exports = Right; 