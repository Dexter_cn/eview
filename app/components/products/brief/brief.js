var React = require('react');
import styles from './brief.css';


var EV07S = [
    {
        "title"  : "Personal/Asset Tracking System",
        "text"   : "Safety for independent and active seniors.",
        "text2"  : "With the ev-07s you can go anywhere knowing help is just a pressof a button away. It is perfect for seniors who like to be independent and active.By not relying on landlines or the NBN you can avoid the hassles of traditional  personal alarms.It is a stylish pendant that works using the same cellular phonetechnology as a mobile phone, but without the complicated features.",
        "img"    : "4.png",
    }
]

var EV09 = [
    {
        "title"  : "EV-09   GPS/GSM/Bluetooth Multi-Purpose Tracker",
        "text"   : "Safety for independent and active seniors.",
        "text2"  : "The EV-09 is a small and powerful Multi-Purpose Tracker, its deveoped by Eview and newly released in 2016, the unique design ensures that the tracker can be used for Pet, Bicycle and People. Its completely portable and quick installation. equipped with Bluetooth for indoor tracking. ",
        "img"    : "4.png",
    }
]

var EV200 = [
    {
        "title"  : "EV-200  Pet GPS Tracker",
        "text"   : "Safety for independent and active seniors.",
        "text2"  : "The EV-200 Portable GPS tracker is designed for monitoring and protecting pet. In case of  emergency, using advanced GPS and wireless technology, you can easily keep track of  your dog's whereabouts. This device is a waterproof IP66, innovativeminiature size pet remote positioning device with built-inU-blox GPS and GSM/GPRS technology.",
        "img"    : "4.png",
    }
]






class Brief extends React.Component {

    render() {
        let data = this.props.data == 'EV07S' ? EV07S : (this.props.data== 'EV09' ? EV09 : EV200);
        return (

            <div id="brief">
            	{
                    data.map((e,index)=>{
                    return (
                        <div key={index}>
                            <div  className="container">
                                <div className="col-sm-6">
                                    <ul>
                                        <li><span>{this.props.data}</span>{e.title}</li>
                                        <li>{e.text}</li>
                                        <li>{e.text2}</li>
                                    </ul>
                                </div>
                                <div className="col-sm-6">
                                    <img src={"img/products/"+this.props.data+"/"+e.img} alt="" width="100%" />
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}



module.exports = Brief; 