var React = require('react');
import styles from './details.css';


var EV07S = [
    {
        "img1"   : "17.png",
        "img2"   : "18.png",
        "content" :[
            {
                "title":"GSM/GPRS Specifications:",
                "text":['Frequency: 850/900/1800/1900 MHz','GPRS: GPRS multi-slot class 12','            GPRS mobile station class B','Communication: Embedded TCP/ IP over GPRS class 10, SMS messages, Voice ','Antenna: Built in FPC antenna' ] 
            },
            {
                "title":"GPS Specifications:",
                "text":['GPS chipset: UBlox G7020 (AGPS Support)Channels: 50','Receiver frequency: 1575.42 MHz','Cold starts: approx 32S, typical TTFF (95%)','Warm start: approx 32S, typical TTFF (95%)' ,'Hot start: approx: 1S, typical TTFF (95%)','Antenna: Built-in ceramic antenna'] 
            },
            {
                "title":"General Specifications:",
                "text":['Dimension:  61mm * 44mm * 16mmWeight: 35g','Backup battery: Rechargeable, 3.7V, 850mAh(Li-Polymer)','Charging voltage: 5V DC','Operating Temperature: ','-20°C to +55°C for working','-30°C to +70°C for storage','Standby Time: 2~7 days','Water Resistance: Follow the IPX5 Standard'] 
            },
            {
                "title":"Hardware Specifications: ",
                "text":['Sensors: Built in 3D motion sensor','                Built in Vibrator sensor','Connectors: Micro USB connectors','SIM card: Micro SIM card, inbuilt','SIM chip accepted','Flash Memory: Built in 1MB memory','Normal current consumption: 10~30mAh','Sleep current consump-tion: 1~2mAh (GPS off)'] 
            }
            
        ]
    }
]

var EV09 = [
    {
        "img1"   : "17.png",
        "img2"   : "18.png",
        "content" :[
            {
                "title":"GSM/GPRS Specifications:",
                "text":['Frequency: Quad-Band 850/900/1800/1900 MHz','GPRS: GPRS multi-slot class 12','            GPRS mobile station class B','Communication: Embedded TCP/IP over','            GPRS class 10, SMS messages, Voice','Antenna: Built in radium carved'] 
            },
            {
                "title":"GPS Specifications:",
                "text":['GPS chipset: MT3332N (AGPS support)','Acquisition: -148 dBm (cold)','                       -163 dBm (hot)','Tracking: -165 dBm','Antenna: Built-in radium carved antenna',' '] 
            },
            {
                "title":"General Specifications:",
                "text":['Dimension: 41mm * 13mm','Weight: 25g','Backup battery: Rechargeable, 3.7V, 280mAh (Li-Polymer)','Charging voltage: 5V DC','Operating Temperature:-20°C to +55°C for working','                                            -30°C to +70°C for storage','Standby Time: 1~3 days','Water Resistance: Follow the IPX7 Standard'] 
            },
            {
                "title":"Hardware Specifications: ",
                "text":['Sensors: Built in Bluetooth','                Built in 3D motion sensor','SIM card: Embedded SIM chip','Flash Memory: Built in 1MB memory','Normal power consumption: 10~30mAh','Sleep power consumption:1~2mAh (GPS off)'] 
            }
            
        ]
    }
]

var EV200 = [
    {
        "img1"   : "17.png",
        "img2"   : "18.png",
        "content" :[
            {
                "title":"GSM/GPRS Specifications:",
                "text":['Frequency: 850/900/1800/1900 MHz','GPRS:GPRS multi-slot class 12,','           GPRS mobile station class B','Communication: Embedded TCP/IP  over GPRS class 10, SMS messages,  Voice','Antenna: Built in FPC antenna'] 
            },
            {
                "title":"GPS Specifications:",
                "text":['GPS chipset: UBlox G7020 (AGPS Support)','Channels: 50','Receiver frequency: 1575.42 MHz','Cold starts: approx 32S, typical TTFF (95%)','Warm start: approx 32S, typical TTFF (95%)','Hot start: approx: 1S, typical TTFF (95%)','Antenna: Built-in ceramic antenna'] 
            },
            {
                "title":"General Specifications:",
                "text":['Dimension:  61mm * 35mm * 16mm','Weight:38g','Battery: Rechargeable, 3.7V, 850mAh Li-Polymer)','Charging voltage: 5V DC','Operating Temperature:-20°C to +55°C for working','                                            -30°C to +70°C for storage','Standby Time: 2~7 days','Water Resistance: Follow the IPX5 Standard'] 
            },
            {
                "title":"Hardware Specifications: ",
                "text":['Sensors:  Built in 3D motion sensor ','                 Built in Vibrator sensor','Connectors: Micro USB connectors ','SIM card: Micro SIM card, inbuilt','                  SIM chip accepted','Flash Memory: Built in 1MB memory','Normal current consumption: 10~30mAh','Sleep current consump-tion:  1~2mAh (GPS off)'] 
            }
            
        ]
    }
]


class Content extends React.Component {
    render(){
        return(
            <ul className="clearfix">
                {this.props.data.map((e,index)=>{
                    return(
                        <li key={index} className="col-sm-5 col-sm-offset-1">
                            <span>{e.title}</span> 
                            {e.text.map((e,index)=>{
                                return (
                                    <div className="text" key={index}>
                                        <p>{e}</p>
                                    </div>
                                )
                            })}
                        </li>
                    )
                })}
            </ul>
        )
    }
}


class Details extends React.Component {

    render() {
        let data = this.props.data == 'EV07S' ? EV07S : (this.props.data== 'EV09' ? EV09 : EV200);
        return (

            <div id="details">
                {
                    data.map((e,index)=>{
                    return (
                        <div key={index}>
                            <div  className="container">
                                <div className="row">
                                   <div className="col-sm-6"><img src={"img/products/"+this.props.data+"/"+e.img1} alt="" /></div>
                                   <div className={this.props.data == 'EV09'?'col-sm-6 positionImg':'col-sm-6'}><img src={"img/products/"+this.props.data+"/"+e.img2} alt="" /></div>
                                   <Content data={e.content} />
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}



module.exports = Details; 