var React = require('react');
import styles from './trait.css';


var EV07S = [
    {
        "title"   : "Features",
        "content" :[
            {
                "title":"Personalized  Fashion Design and Colorful  ",
                "text":"A  true combination  of style and technology,mini  size  and light  weight，rubber handle paint material.",
                "img":"5.png"
            },
            {
                "title":"Simple Command  and User-fiendly  ",
                "text":"Simple SMS command  and case are not sensitive.Gently knock the device twice activates  the LED ON/OFF，Easy and funto  use.",
                "img":"6.png"
            },
            {
                "title":"Two  Way Voice Communication",
                "text":"To be able to the one  you  care  about.",
                "img":"7.png"
            },
            {
                "title":"Fall  Detection for Child , Patient and Elderly",
                "text":"A simple fall may cause a  devastating consequence . A Fall  Sensor can help you get  immediate  help  without  missing  any important moment.",
                "img":"8.png"
            }
        ],
        "item":[
            'Real time tracking and monitoring.',
            'High GPS accuracy and AGPS support.',
            'Simple help alarm button activation.',
            'Docking station for easy charging.',
            'Fall detection.',
            'GEO safe zone.',
            'No movement alarm.',
            'Voice monitoring.',
            'Low GPRS data flow.',
            '5 working modes for power management.',
            'Re-upload data for blind area.',
            'TCP/IP, UDP protocol are available.',
            'OTA (Firmware upgrade over the air).'

        ]
    }
]

var EV09 = [
    {
        "title"   : "Outstanding advantages",
        "content" :[
            {
                "title":"Quick and Easy installation",
                "text":"Silicone belt enables you attach to any collar, bike or cloths easily. The clip enables you click it everywhere. ",
                "img":"5.png"
            },
            {
                "title":"Small, Light, Waterproof",
                "text":"Super small size and weight just 25grams,  follow the IPX7 waterproof standard.",
                "img":"6.png"
            },
            {
                "title":" Embedded SIM Chip ",
                "text":"Equipped with embedded SIM chip, safe and secure, no need to put SIM card inside, it also can’t be taken out from EV-09.",
                "img":"7.png"
            },
            {
                "title":"Bluetooth 4.0 for Indoor Tracking",
                "text":"Paring to our docking station or smart mobile phone when GPS  is not available in room. Perfect for monitor elderly  person.",
                "img":"8.png"
            }
        ],
        "item":[
            'GPS/BLE Tracking.',
            'SOS help alarm.',
            '2-way calling.',
            'Geo safe zone.',
            'Anti-theft for bike.',
            'IOS and Android APP.',
        ]
    }
]

var EV200 = [
    {
        "title"   : "Features",
        "content" :[
            {
                "title":" Waterproof design",
                "text":"A  true combination  of style and technology,mini  size  and light  weight，rubber handle paint material.",
                "img":"5.png"
            },
            {
                "title":"A-GPS Technology(By u-Blox)",
                "text":"A - GPS technology is based on GPS, it can achieve rapid positioning, through the base station of mobile communication operation for formalis assisted global positioning system.",
                "img":"6.png"
            },
            {
                "title":"Inbuilt microphone and speaker",
                "text":"You can send messages to your pet at the speaker.",
                "img":"7.png"
            },
            {
                "title":"Supports SMS & GPRS Tracking",
                "text":"Real-time GPS tracking, pet out of the fence will be issued after the  alarm message.",
                "img":"8.png"
            }
        ],
        "item":[
            'Long battery working time.',
            'GSM/GPRS simultaneously.',
            'Built-in 3D G-sensor for motion, shock alarm and power management.',
            'Voice wiretapping.',
            'Two way Voice communication.',
            'Built in 8MB flash memory. ',
            'Free Real time online tracking.',
            'Data logging: 60000locations.',
            'GPRS blind area data re-upload function.',
            'Firmware upgrade over the air. ',
            'Reply map link of current position.',
            'Geo-fencing alarm, Over speed alarm.',
            'Micro USB port.'

        ]
    }
]


class Content extends React.Component {
    render(){
        return(
            <ul className="clearfix">
                {this.props.data.map((e,index)=>{
                    return(
                        <li key={index} className="col-sm-6">
                            <p>{e.title}</p>
                            <p>{e.text}</p>
                            <p><img src={"img/products/"+this.props.name+"/"+e.img} alt="" /></p>
                        </li>
                    )
                })}
            </ul>
        )
    }
}

class ItemShow extends React.Component {
    render(){
        return(
            <ul className="clearfix itemShow">
                {this.props.data.map((e,index)=>{
                    return(
                        <li key={index} className="col-sm-6">{e}</li>
                    )
                })}
            </ul>
        )
    }
}



class Trait extends React.Component {

    render() {
        let data = this.props.data == 'EV07S' ? EV07S : (this.props.data== 'EV09' ? EV09 : EV200);
        return (

            <div id="trait">
            	{
                    data.map((e,index)=>{
                    return (
                        <div key={index}>
                            <div  className="container">
                                <div className="row">
                                    <div className="title"><span>01</span> {e.title}</div>
                                    <Content data={e.content} name={this.props.data} />
                                    <ItemShow data={e.item}   name={this.props.data} />
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        );
    }
}



module.exports = Trait; 