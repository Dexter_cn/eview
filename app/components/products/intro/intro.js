var React = require('react');
import styles from './intro.css';


var EV07S = [
    {
        "title"  : "Personal/Asset Tracking System",
        "text"   : "Mini sized, Waterproof IP66",
        "text2"  : "Real time tracking by GPS satellite",
        "img"    : "1.png",
        "btn"    : "Order Now",
        "show"   : "2.png",
        "fullimg": "3.jpg"
    }
]

var EV09 = [
    {
        "title"  : "GPS/GSM/Bluetooth",
        "text"   : "Multi-Purpose Tracker",
        "text2"  : "Mini sized, Waterproof IP66Real time tracking by GPS satellite",
        "img"    : "",
        "btn"    : "Order Now",
        "show"   : "2.png",
        "fullimg": "3.jpg"
    }
]

var EV200 = [
    {
        "title"  : "Pet GPS Tracker",
        "text"   : "Mini sized, Waterproof IP66",
        "text2"  : "Real time tracking by GPS satellite",
        "img"    : "",
        "btn"    : "Order Now",
        "show"   : "2.png",
        "fullimg": "3.jpg"
    }
]






class Intro extends React.Component {

    render() {
        let data = this.props.data == 'EV07S' ? EV07S : (this.props.data== 'EV09' ? EV09 : EV200);
        return (

            <div id="Intro">
            	{
                    data.map((e,index)=>{
                    return (
                        <div key={index}>
                            <div  className="container">
                                <div className="col-sm-6">
                                    <ul>
                                        <li>{e.title}</li>
                                        <li>{e.text}<br />{e.text2}</li>
                                        <li>{e.img ==''?'':<img src={"img/products/"+this.props.data+"/"+e.img} alt="" />}</li>
                                        <li><button className="btn btn-primary">{e.btn}</button></li>
                                    </ul>
                                </div>
                                <div className="col-sm-6">
                                    <img src={"img/products/"+this.props.data+"/"+e.show} alt="" />
                                </div>
                            </div>
                            <div className="container-fluid "><img src={"img/products/"+this.props.data+"/"+e.fullimg} width="100%" /></div>
                        </div>
                    )
                })}
            </div>
        );
    }
}



module.exports = Intro; 