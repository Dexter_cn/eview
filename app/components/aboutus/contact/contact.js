var React = require('react');
import styles from './contact.css';

function initMap() {
  var myLatLng = {lat:22.641585, lng:114.029524};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Eview'
  });
}

class Contact extends React.Component {

    

    componentDidMount() {
         initMap(); 
    }

    render() {
        return (

            <div id="contact" className="container">
              	<div className="title">
              		<span>Contact Us</span>
              		<p>Contact Eview today to discuss your needs and how they may be able to help. Who knows, they may have already deployed a solution to match your specific requirement.</p> 
              	</div>

                  
                <div id="map" className="container"></div>


                  

              	<div className="row">
              		<div className='col-sm-3'>
              			<div>
              				<img src="img/about/7.png" alt="" />
              				<dl>
              					<dt>Add</dt>
              					<dd>Room 201, B5 Building, Zhongsheng</dd>
              					<dd>Industrial Area,Gongye Road, </dd>
              					<dd>Longhua district, Shenzhen City, China</dd>
              				</dl>
              			</div>
              		</div>
              		<div className='col-sm-3'>
              			<div>
              				<img src="img/about/8.png" alt="" />
              				<dl>
              					<dt>Telephone number</dt>
              					<dd>+86-755-23772735/28073450 </dd>
              					<dd>+86-755-33100549</dd>
              					<dd><b>Fax</b>:+86-755-28073450</dd>
              				</dl>
              			</div>
              		</div>
              		<div className='col-sm-3'>
              			<div>
              				<img src="img/about/9.png" alt="" />
              				<dl>
              					<dt>Email</dt>
              					<p className="borderright">
              						<dd><b>Sales Department:</b></dd>
	              					<dd>sales@eviewltd.com</dd>
	              					<dd>info@eviewltd.com</dd>
              					</p>
              					<p>
              						<dd><b>Sales Department:</b></dd>
	              					<dd>sales@eviewltd.com</dd>
	              					<dd>info@eviewltd.com</dd>
              					</p>
              				</dl>
              			</div>
              		</div>
              		<div className='col-sm-3'>
              			<div>
              				<img src="img/about/10.png" alt=""/>
              				<dl>
              					<dt>Emergency call</dt>
              					<dd>+86-18620310172</dd>
              					<dd>+86-15899795842</dd>
              				</dl>
              			</div>
              		</div>
              	</div>
            </div>
        );
    }
}



module.exports = Contact; 