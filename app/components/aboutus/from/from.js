var React = require('react');
import styles from './from.css';


class From extends React.Component {

    render() {
        return (

            <div id="from">
            	<from>
            	 	<div className="container">
            	 		<div className="row">
            	 			<div className="col-sm-12 title">
            	 				<p>Stay tuned with the latest Eview updates!</p> 
            	 				<p>If you want to subscribe to our newsletter, please submit the form below.</p> 
            	 			</div>
            	 		</div>

            	 		<div className="row content">
            	 			<div className="col-sm-6">

            	 				<div className="row">
            	 					<div className="col-sm-4">
            	 						<p>Name</p>
            	 						<input name="name" placeholder="Name" type="text" />
            	 					</div>
            	 					<div className="col-sm-4">
            	 						<p>E-mail</p>
            	 						<input name="mail" placeholder="Email" type="text" />
            	 					</div>
            	 					<div className="col-sm-4">
            	 						<p>Number</p>
            	 						<input name="Number" placeholder="Number" type="text" />
            	 					</div>
            	 				</div>

            	 				<div className="row">
            	 					<div className="col-sm-4">
            	 						<p>Company</p>
            	 						<input name="Company" placeholder="Company" type="text" />
            	 					</div>
            	 					<div className="col-sm-4">
            	 						<p>Website</p>
            	 						<input name="Website" placeholder="Website" type="text" />
            	 					</div>
            	 					<div className="col-sm-4">
            	 						<p>Your Title</p>
            	 						<input name="Title" placeholder="Title" type="text" />
            	 					</div>
            	 				</div>

            	 				<div className="row">
            	 					<div className="col-sm-12">
            	 						<p>Your Central Station (if Applicable)</p>
            	 						<input name="centralStation" placeholder="Your Central Station" type="text" />
            	 					</div>
            	 				</div>
            	 			</div>

            	 			<div className="col-sm-6">
            	 				<div className="row">
            	 					<div className="col-sm-12">
            	 						<p>Message</p>
            	 						<textarea name="Message" placeholder="Message" rows="6"></textarea>	
            	 					</div>
            	 				</div>

            	 				<div className="row">
            	 					<div className="col-sm-12">
            	 						<button className="btn btn-primary btn-block">Submit</button>            	 						
            	 					</div>
            	 				</div>
            	 			</div>

            	 		</div>

            	 	</div>
            	</from>
            </div>
        );
    }
}



module.exports = From; 