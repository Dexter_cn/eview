var React = require('react');


let Intro   = require('./intro/intro.js');
let About   = require('./about/about.js');
let Office  = require('./office/office.js');
let Contact = require('./contact/contact.js');
let From    = require('./from/from.js');

//ES6
class Aboutus extends React.Component {
    render() {
        return (
        	<div id="aboutus" className="contentFirst">
                <Intro   />
                <About   />
                <Office  />
                <Contact  />
                <From  />
            </div>
        );
    }
}

module.exports = Aboutus;