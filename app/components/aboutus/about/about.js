var React = require('react');
import styles from './about.css';


class About extends React.Component {

    render() {
        return (

            <div id="About" className="container">
            	<div className="col-lg-7">
	        		<p><span>Eview</span> is a professional consumer GPS (Global Positioning System) products provider. Eview designs,develops,and manufactures high quality GPS tracking products for B2B and  OEM/ODM customers, including Vehicle and personal tracking systems. </p>
	        		<img src="img/about/2.png" alt=""  />
	        		<p>We have a R&D team with 10 experienced engineers,a manufacturing factory with <b>ISO9001:2008</b> certified. The Eview development team are  proud of this reputation which not only  boasts absolute flexibility, but also highlights their ability to come up with the perfect solutionfor all their  customers irrespective of how unique or obscurethe customer need. </p> </div>
            	<div className="col-lg-4 clearfix">
            		<img src="img/about/3.png" alt=""  />
            	</div>
            </div>
        );
    }
}



module.exports = About; 