var React = require('react');
import styles from './office.css';



class Office extends React.Component {

    render() {
        return (

            <div id="office" className="container">
              <div className="col-sm-4">
              	<p>Office</p>
              	<img src="img/about/4.png" alt="" width="100%" />
              </div>
              <div className="col-sm-4">
              	<p>Factory</p>
              	<img src="img/about/5.png" alt="" width="100%" />
              </div>
              <div className="col-sm-4">
              	<p>Exhibition</p>
              	<img src="img/about/6.png" alt="" width="100%" />
              </div>
            </div>
        );
    }
}



module.exports = Office; 