var React = require('react');
import styles from './ev07.css';


class EV07 extends React.Component {
    render() {
        return (
            <div id={this.props.id}  className="container">
            	<h1 className="title-all">EV-07 Features</h1>
                <div className="row">
                    <EV07Left />
                    <EV07Right />
                </div>
            </div>
        );
    }
}


class EV07Left extends React.Component {
    render(){
        return(
            <div className="col-lg-7">
                <ul>
                    <li>
                        <img className="img-box" src="img/home/ev07/ev_img_1.jpg" alt="" width="100%" />
                    </li>
                    
                    <li className="img-text">The EV-07W is super small 3G(WCDMA) personal/asset GPS tracker. It is designed for monitoring and protecting people and property. keeping busy families connected with tracking information and voice functionality. Besides, it built in 3D G-sensor that can be used for no movement alarm, elderly falling, power management, and more.</li>
                </ul>
            </div>
        )
    }
}


class EV07Right extends React.Component {
    render(){
        return(
            <div className="col-lg-5">
                <ul className="ev07-right">
                   {
                      dataEV07Right.map((e,index) => {
                        return(
                            <li key={index} className={e.img == '' ? 'noImg row' : 'row'}>
                                {e.img == '' ? '' : <div className="col-sm-3"><img src={"img/home/ev07/"+e.img} /></div>}
                                <div className={e.img == '' ? 'col-sm-12' : 'col-sm-9'}>
                                    <h2>{e.title}</h2>
                                    <p>{e.text}</p>
                                </div>
                            </li>
                        )
                      })  
                   }
                </ul>
            </div>
        )
    }
}


var dataEV07Right = [
    {
        "img":'right_2g3g.png',
        "title":'3G(WCDMA) network',
        "text":'Small size 3G personal GPS tracker.'
    },
    {
        "img":'right_jb.png',
        "title":'Safety alerts',
        "text":'SOS button, Geo safe zone, No Movement alert.'
    },
    {
        "img":'right_sd.png',
        "title":'Fall detection',
        "text":'Two-way calling . With built in MIC and speaker,two-way calling to talk anytime.'
    },
    {
        "img":'right_gps.png',
        "title":'GPS Tracking',
        "text":'Real time tracking and monitoring, High GPS Accuracy and AGPS support.'
    },
    {
        "img":'',
        "title":'Easy Charging',
        "text":'A docking station provides an alternative method to charge and make it a lot quicker and easy to use for elderly.'
    },
    {
        "img":'',
        "title":'Portable Design',
        "text":'Compact size and easy to use, the weight is only 40g.'
    },
    {
        "img":'',
        "title":'Water resistance',
        "text":'Follow the IPX5 standard'
    },
    {
        "img":'',
        "title":'Free App and Accessible',
        "text":'User-friendly app that is intuitive and easy to use.'
    }
]; 



module.exports = EV07; 