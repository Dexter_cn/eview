var React = require('react');
import styles from './evapp.css';

//ES6
class EVapp extends React.Component {
    render() {
        return (
            <div id={this.props.id} className="container-fluid">
            	<div className="container">
            		<div className="row">
            			<dvi className="col-lg-6">
            				<p className="titleP">Add as many tracker as you want and share them  with your whole family.</p>
            				<ul>
            					<li>
            						<a href="javascript:void(0)"><img src="img/home/evapp/download_app.png" alt="" /></a>
            						<a href="javascript:void(0)"><img src="img/home/evapp/download_gg.png" alt="" /></a>
            					</li>
            					<li>
            						<span>Share tracker</span>
            						<p>When you set up your tracker you can easily share it with anyone whouses the Eview  App. You are in control at all times. It's very easy to toggle the sharing for individual trackers and followers.</p>
            					</li>
            					<li>
            						<span>Multiple Tracker support</span>
            						<p>Eview  comes with a free App that is intuitive and easy to use. You will be able to follow your trackers in real-time and there is no limit to howmany trackers you can add.</p>
            					</li>
            				</ul>
            			</dvi>
            			<div className="col-lg-6">
            				<img className="titleImg" src="img/home/evapp/ev_app.png" width="100%"/>
            			</div>
            		</div>
            	</div>
            </div>
        );
    }
}


module.exports = EVapp; 