var React = require('react');
import styles from './company.css';


class Company extends React.Component {
    render() {
        return (
            <div id={this.props.id}  className="container">
            	<h1 className="title-all">Company introduction</h1>
            	<p>Eview is a professional consumer GPS (Global Positioning System) products provider.</p>
            	<p> Eview designs,develops,and manufactures high quality GPS tracking products for B2B </p>
            	<p>and OEM/ODM customers, including Vehicle and personal tracking systems.</p>
            </div>
        );
    }
}



module.exports = Company; 