var React = require('react');
import styles from './presonal.css';

var dataPresonal = [
	{
		"img":"img/home/presonal/pre_dl.png",
		"title":"Personal Tracking System",
		"Model":"EV-07S",
		"text":" Mini sized, Waterproof IP66, Rubber coating, feels comfortable.Real time tracking by GPS satellite."
	},
	{
		"img":"img/home/presonal/pre_map.png",
		"title":"Personal Tracking System",
		"Model":"EV-07S",
		"text":" Mini sized, Waterproof IP66, Rubber coating, feels comfortable.Real time tracking by GPS satellite."
	},
	{
		"img":"img/home/presonal/pre_dog.png",
		"title":"Personal Tracking System",
		"Model":"EV-07S",
		"text":" Mini sized, Waterproof IP66, Rubber coating, feels comfortable.Real time tracking by GPS satellite."
	}
];

class Presonal extends React.Component {
    render() {
        return (
            <div id={this.props.id}  className="container-fluid bgItem">
            	<div className="container">
            		{dataPresonal.map((e,index)=>{
            			return (
            					<div className="col-sm-4" key={index}>
            						<p><img src={e.img} alt="" /></p>
            						<h3>{e.title}</h3>
            						<h3>{e.Model}</h3>
            						<p>{e.text}</p>
            					</div>
            				)
            		})}
            	</div>
            </div>
        );
    }
}



module.exports = Presonal; 