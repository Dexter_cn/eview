var React = require('react');
import styles from './carousel.css';

//ES6
class Carousel extends React.Component {
    render() {
        return (
            <div id={this.props.id} className="container-fluid">
            	<img src="img/index_2.jpg" alt="" width="100%" />
            </div>
        );
    }
}

module.exports = Carousel; 