var React = require('react');
import styles from './ev200.css';

//ES6
class EV200 extends React.Component {
    render() {
        return (
            <div id={this.props.id} className="container">
            	<h1 className="title-all">EV-200  Features</h1>
            	<div className="row">
            		<div className="row">
                        <div className="col-md-6">
            			     <p>The EV-200 Portable GPS tracker is designed for monitoring and protecting pet. In case of  emergency, using advanced GPS and wireless technology, you can easily keep track of  your dog's whereabouts. This device is a waterproof IP66, innovative miniature size pet remote positioning device with built-in U-blox GPS and GSM/GPRS technology.</p>
                        </div>
                         <div className="col-md-6">
                            <img src="img/home/ev200/ev200-dog.png" width="100%" />
                        </div>
            		</div>
            		<div className="row">
                        <div className="col-md-6 ev200List">
                            <img className="ev200yb" src="img/home/ev200/ev200-yb.png" />
                            <ul>
                            {
                                data.map((e,index) => {
                                    return (
                                        <li key={index}>{e.title}</li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                        <div className="col-md-6">
                            <img className="ev200Item" src="img/home/ev200/ev200.png" alt=""  />
                        </div>
            		</div>
            	</div>
            </div>
        );
    }
}


var data = [
	{
		"title":"A-GPS Technology(By u-Blox)"
	},
	{
		"title":"3D G-sensor"
	},
	{
		"title":"Inbuilt microphone and speaker"
	},
	{
		"title":"Battery life u to 3 days"
	},
	{
		"title":"Supports SMS&GPRS Tracking"
	},
	{
		"title":"Waterproof design"
	}
]

module.exports = EV200; 