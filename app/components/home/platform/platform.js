var React = require('react');
import styles from './platform.css';


class Platform extends React.Component {
    render() {
        return (
            <div id={this.props.id}  className="container-fluid bgItem">
            	 <div className="container">
            	 	<div className="row">
            	 		<div className="col-md-8">
            	 			<h1>SMART GPS Tracking Platform</h1>
            	 			<p>IOS and Android. The app allows you to track the device and receive alerts, keeping you in touch with your loved ones at the click of a button.</p>
            	 		</div>
            	 		<div className="col-md-4 computer">
							<img src="img/home/platform/computer.png" />
            	 		</div>
            	 	</div>

            	 	<div className="row">
            	 		{data.map((e,index) => {
            	 			return (
            	 				<div key={index} className="platform-icon">
            	 						<img src={e.img} alt="" />
            	 						<h4>{e.title}</h4>
            	 						<p>{e.text}</p>
            	 				</div>
            	 			)
            	 		})}
            	 	</div>

            	 </div>
            </div>
        );
    }
}


var data = [
	{
		"img":"img/home/platform/item_user.png",
		"title":"Account Control Management",
		"text" :"Create and manage accounts for your  customers or members without limits."
	},
	{
		"img":"img/home/platform/item_ui.png",
		"title":"Smart UI Design",
		"text" :"Freely change the user interface following your own habits."
	},
	{
		"img":"img/home/platform/item_wirte.png",
		"title":"Valuable Reports",
		"text" :"Our software can generate various & critical reports."
	},
	{
		"img":"img/home/platform/item_computer.png",
		"title":"Multi-Screen Monitoring",
		"text" :"View tracker’s location from yourPC , laptop or mobile phone."
	},
	{
		"img":"img/home/platform/item_mail.png",
		"title":"Email Notification",
		"text" :"Our Software can send alarm reports on your e-mail address."
	},
	{
		"img":"img/home/platform/item_logo.png",
		"title":"Rebranding Accepted",
		"text" :"Quickly start tracking business ata very low investment."
	},
	{
		"img":"img/home/platform/item_font.png",
		"title":"Multi-Language",
		"text" :"Our Tracking Software is available in more than 13 popula languages.More language  versions arebeing added."
	},
	{
		"img":"img/home/platform/item_time.png",
		"title":"24/7 Technical Support",
		"text" :"Provide 24/7 technical service"
	}
];



module.exports = Platform; 