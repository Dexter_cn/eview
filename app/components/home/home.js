var React = require('react');
let Carousel = require('./carousel/carousel.js');
let Company = require('./company/company.js');
let Presonal = require('./presonal/presonal.js');
let EV07 = require('./ev07/ev07.js');
let EV200 = require('./ev200/ev200.js');
let EVapp = require('./evapp/evapp.js');
let Platform = require('./platform/platform.js');



//ES6
class Home extends React.Component {
    render() {
        return (
            <div>
            	<Carousel id="carousel" />
            	<Company  id="company" />
            	<Presonal  id="presonal" />
            	<EV07  id="ev07" />
                <Platform  id="platform" />
                <EV200  id="ev200" />
                <EVapp  id="evapp" />
            </div>
        );
    }
}

module.exports = Home; 